/**********************************************************************************************
 Copyright (c) 2015 DisplayModule. All rights reserved.

 Redistribution and use of this source code, part of this source code or any compiled binary
 based on this source code is permitted as long as the above copyright notice and following
 disclaimer is retained.

 DISCLAIMER:
 THIS SOFTWARE IS SUPPLIED "AS IS" WITHOUT ANY WARRANTIES AND SUPPORT. DISPLAYMODULE ASSUMES
 NO RESPONSIBILITY OR LIABILITY FOR THE USE OF THE SOFTWARE.
 ********************************************************************************************/

#ifndef DM_OLED_Us2066_h
#define DM_OLED_Us2066_h

#include "DmTftBase.h"

#define OLED_I2C_ADDRESS 0x3C
#define OLED_COMMAND 0x80
#define OLED_DATA 0x40


class DmOledUs2066 : public DmTftBase
{
public:
  DmOledUs2066();
  virtual ~DmOledUs2066();
  void init(uint8_t x, uint8_t y);
  void setCursor(uint8_t x, uint8_t y);
  void drawString(uint8_t x, uint8_t y, const char *p);
  void scrollString(uint16_t y, char *msg, uint32_t time);
  void clearOled();
  
  

private:
	
  virtual void setAddress(uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1);
  virtual void sendCommand(uint8_t index);
  virtual void sendData(uint16_t data);
  virtual void send8BitData(uint8_t data);
  
  uint16_t _cols;
  uint16_t _rows;


};


#endif

