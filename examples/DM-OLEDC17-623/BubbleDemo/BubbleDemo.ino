/**********************************************************************************************
 Copyright (c) 2015 DisplayModule. All rights reserved.

 Redistribution and use of this source code, part of this source code or any compiled binary
 based on this source code is permitted as long as the above copyright notice and following
 disclaimer is retained. 

 DISCLAIMER:
 THIS SOFTWARE IS SUPPLIED "AS IS" WITHOUT ANY WARRANTIES AND SUPPORT. DISPLAYMODULE ASSUMES
 NO RESPONSIBILITY OR LIABILITY FOR THE USE OF THE SOFTWARE.
 ********************************************************************************************/
#include <SPI.h>
#include <Wire.h>
#include <DmOledSeps525.h>
#include <utility/BubbleDemo.h>

#define F_CS    6

DmOledSeps525 oled = DmOledSeps525(10, 9, 8);
BubbleDemo bubbleDemo(&oled, oled.width(), oled.height());

void setup ()
{
  // Set CS SPI pin HIGH for all SPI units, so they don't interfere
  pinMode(F_CS, OUTPUT);
  digitalWrite(F_CS, HIGH);
  
  oled.init();
}

void loop()
{
  bubbleDemo.run(750, 20);
}


