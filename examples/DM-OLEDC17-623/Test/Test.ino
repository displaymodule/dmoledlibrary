/**********************************************************************************************
 Copyright (c) 2015 DisplayModule. All rights reserved.

 Redistribution and use of this source code, part of this source code or any compiled binary
 based on this source code is permitted as long as the above copyright notice and following
 disclaimer is retained. 

 DISCLAIMER:
 THIS SOFTWARE IS SUPPLIED "AS IS" WITHOUT ANY WARRANTIES AND SUPPORT. DISPLAYMODULE ASSUMES
 NO RESPONSIBILITY OR LIABILITY FOR THE USE OF THE SOFTWARE.
 ********************************************************************************************/
#include <SPI.h>
#include <SPIFlash.h>
#include <SD.h>
#include <Wire.h>
#include <DmOledSeps525.h>
#include <DmDrawBmpFromProgmem.h>



DmOledSeps525 oled = DmOledSeps525(10, 9, 8);

void setup()
{
 
  Serial.begin(9600);
  oled.init();
  
  oled.drawString(40, 10, "Hello World!"); 
  oled.drawString(0, 80, "www.displaymodule.com");
  delay(3000);
  
  uint32_t startTime, endTime;
  
  startTime = millis();
  Rainbow();
  endTime = millis();
  Serial.print("Draw Image: ");
  Serial.print(endTime - startTime, DEC);
  Serial.println(" ms");
}

void loop() { }

void Rainbow()
{
  oled.fillRectangle(0x00, 0x00, 0x13, 127, 0xFFFF);
  oled.fillRectangle(0x14, 0x00, 0x27, 127, 0xFFE0);
  oled.fillRectangle(0x28, 0x00, 0x3B, 127, 0xF81F);
  oled.fillRectangle(0x3C, 0x00, 0x4F, 127, 0x07FF);
  oled.fillRectangle(0x50, 0x00, 0x63, 127, 0xF800);
  oled.fillRectangle(0x64, 0x00, 0x77, 127, 0x07E0);
  oled.fillRectangle(0x78, 0x00, 0x8B, 127, 0x001F);
  oled.fillRectangle(0x8C, 0x00, 0x127, 127, 0x0000);
}
