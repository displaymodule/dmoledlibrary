/**********************************************************************************************
 Copyright (c) 2015 DisplayModule. All rights reserved.

 Redistribution and use of this source code, part of this source code or any compiled binary
 based on this source code is permitted as long as the above copyright notice and following
 disclaimer is retained. 

 DISCLAIMER:
 THIS SOFTWARE IS SUPPLIED "AS IS" WITHOUT ANY WARRANTIES AND SUPPORT. DISPLAYMODULE ASSUMES
 NO RESPONSIBILITY OR LIABILITY FOR THE USE OF THE SOFTWARE.
 ********************************************************************************************/
#include <SPI.h>
#include <SPIFlash.h>
#include <Wire.h>
#include <DmOledUs2066.h>

DmOledUs2066 oled = DmOledUs2066();

void setup()
{ 
  Serial.begin(9600);
  oled.init(16, 2); 
  
  uint32_t startTime, endTime;  
  startTime = millis();
  
  oled.drawString(2, 0, "Hello World!"); 
  oled.drawString(2, 1, "Displaymodule");
  
  endTime = millis();
  Serial.print("Total Time: ");
  Serial.print(endTime - startTime, DEC);
  Serial.println(" ms");  
  delay(2000);
  
}

void loop() {
    oled.scrollString(1, "Hello, www.displaymodule.com", 100);   
  }


