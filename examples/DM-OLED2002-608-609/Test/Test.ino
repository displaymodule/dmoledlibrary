/**********************************************************************************************
 Copyright (c) 2015 DisplayModule. All rights reserved.

 Redistribution and use of this source code, part of this source code or any compiled binary
 based on this source code is permitted as long as the above copyright notice and following
 disclaimer is retained. 

 DISCLAIMER:
 THIS SOFTWARE IS SUPPLIED "AS IS" WITHOUT ANY WARRANTIES AND SUPPORT. DISPLAYMODULE ASSUMES
 NO RESPONSIBILITY OR LIABILITY FOR THE USE OF THE SOFTWARE.
 ********************************************************************************************/
#include <SPI.h>
#include <SPIFlash.h>
#include <Wire.h>
#include <DmOledWs0010.h>

//DmOledWs0010(uint8_t sck, uint8_t miso, uint8_t mosi, uint8_t cs)
DmOledWs0010 oled = DmOledWs0010(13, 12, 11, 10);

void setup()
{
 
  Serial.begin(9600);
  oled.init(20, 2);  
  
  uint32_t startTime, endTime;  
  startTime = millis();
  
  oled.drawString(4, 0, "Hello World!"); 
  oled.drawString(2, 1, "Displaymodule.com");
  
  endTime = millis();
  Serial.print("Total Time: ");
  Serial.print(endTime - startTime, DEC);
  Serial.println(" ms");
  
}

void loop() { }


