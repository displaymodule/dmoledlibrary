/**********************************************************************************************
 Copyright (c) 2015 DisplayModule. All rights reserved.

 Redistribution and use of this source code, part of this source code or any compiled binary
 based on this source code is permitted as long as the above copyright notice and following
 disclaimer is retained.

 DISCLAIMER:
 THIS SOFTWARE IS SUPPLIED "AS IS" WITHOUT ANY WARRANTIES AND SUPPORT. DISPLAYMODULE ASSUMES
 NO RESPONSIBILITY OR LIABILITY FOR THE USE OF THE SOFTWARE.
 ********************************************************************************************/

This is Display Module OLED library which supports it's displays. Have a look at the current displays at www.displaymodule.com.

### History ###

* v1.0
* A lightweight OLED library with support for most modules.


### How to install the library ###

1. Download the library and unzip DmOledLibrary to your Arduino library folder.
   (On Windows it's usually C:\Users\<your name>\Documents\Arduino\libraries)

2. Restart Arduino IDE, now you can find examples in the File->Examples->DmOledLibrary


Please visit www.displaymodule.com for the latest version of this library or if you need any help.