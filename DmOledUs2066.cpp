/**********************************************************************************************
 Copyright (c) 2015 DisplayModule. All rights reserved.

 Redistribution and use of this source code, part of this source code or any compiled binary
 based on this source code is permitted as long as the above copyright notice and following
 disclaimer is retained.

 DISCLAIMER:
 THIS SOFTWARE IS SUPPLIED "AS IS" WITHOUT ANY WARRANTIES AND SUPPORT. DISPLAYMODULE ASSUMES
 NO RESPONSIBILITY OR LIABILITY FOR THE USE OF THE SOFTWARE.
 ********************************************************************************************/

#include "DmOledUs2066.h"

DmOledUs2066::DmOledUs2066():DmTftBase(20, 2)
{
}

DmOledUs2066::~DmOledUs2066() 
{
}
void DmOledUs2066::sendCommand(uint8_t command) 
{
    Wire.beginTransmission(OLED_I2C_ADDRESS);	
    Wire.write(OLED_COMMAND);    		  
    Wire.write(command);    
	Wire.endTransmission();   
	delay(5);
}

void DmOledUs2066::send8BitData(uint8_t data)
{
    Wire.beginTransmission(OLED_I2C_ADDRESS); 	
    Wire.write(OLED_DATA);     		  
    Wire.write(data);    
	Wire.endTransmission();   
}

void DmOledUs2066::sendData(uint16_t data) 
{

}

void DmOledUs2066::setCursor(uint8_t x, uint8_t y)
{
 	uint8_t row_offsets[] ={0x00, 0x40};
	if(y >= _rows){
		y = 0;
		}
	sendCommand((x + row_offsets[y])|0x80);
}
void DmOledUs2066::setAddress(uint16_t x0,uint16_t y0,uint16_t x1,uint16_t y1)
{
}


void DmOledUs2066::drawString(uint8_t x,uint8_t y, const char * p)
{
    setCursor(x, y);
	unsigned char i=0;	
	while(p[i]){    
		send8BitData(p[i]); 	 // *** Show String to OLED    
		i++;  
    }
}

void DmOledUs2066::scrollString(uint16_t y, char * msg, uint32_t time)
{
	char buffer[_cols]; 
	for (byte i=0;i<strlen(msg)+_cols;i++) {	
		uint8_t pos = i+1;  
		for (uint8_t j=0;j<_cols;j++) {	 
			if ((pos<_cols)||(pos>strlen(msg)+_cols-1)) { 
			buffer[j]=' ';   
			}   
			else 
				buffer[j]=msg[pos-_cols];	 
			pos++;  
			} 
			drawString(0, y, buffer); 
			delay(time); 
	}
}

void DmOledUs2066::init(uint8_t x, uint8_t y) {
  _cols = x;
  _rows = y;

  Wire.begin();
  
  // us2066 init
  sendCommand(0x2A); 
  sendCommand(0x71); 
  send8BitData(0x00); 
  sendCommand(0x28); 
  
  sendCommand(0x08);  
  sendCommand(0x2A); 
  sendCommand(0x79);
  
  sendCommand(0xD5);   
  sendCommand(0x70); 
  sendCommand(0x78); 
  
  sendCommand(0x08);	 
  sendCommand(0x06);  
  sendCommand(0x72);	
  send8BitData(0x01);	
  
  sendCommand(0x2A); 	 
  sendCommand(0x79);
  
  sendCommand(0xDA);
  sendCommand(0x10); 
	
  sendCommand(0xDC);	 
  sendCommand(0x00); 
  
  sendCommand(0x81);	 
  sendCommand(0x8F); 
  
  sendCommand(0xD9); 	 
  sendCommand(0xF1);  
  sendCommand(0xDB);  
  sendCommand(0x30);	 
  sendCommand(0x78);	 
  sendCommand(0x28);  
  sendCommand(0x01);  
  sendCommand(0x80);	
  delay(100);
  sendCommand(0x0C);	 
	
}

/*********************************************************************************************************
  END FILE
*********************************************************************************************************/

