/**********************************************************************************************
 Copyright (c) 2015 DisplayModule. All rights reserved.

 Redistribution and use of this source code, part of this source code or any compiled binary
 based on this source code is permitted as long as the above copyright notice and following
 disclaimer is retained.

 DISCLAIMER:
 THIS SOFTWARE IS SUPPLIED "AS IS" WITHOUT ANY WARRANTIES AND SUPPORT. DISPLAYMODULE ASSUMES
 NO RESPONSIBILITY OR LIABILITY FOR THE USE OF THE SOFTWARE.
 ********************************************************************************************/

#ifndef DM_OLED_WS0010_h
#define DM_OLED_WS0010_h

#include "DmTftBase.h"

class DmOledWs0010 : public DmTftBase
{
public:
  DmOledWs0010(uint8_t sck=D13, uint8_t miso=D12, uint8_t mosi=D11, uint8_t cs=D10);
  virtual ~DmOledWs0010();
  void init(uint8_t x, uint8_t y);
  void setCursor(uint8_t x, uint8_t y);
  void drawString(uint8_t x, uint8_t y, const char *p);
  void drawChar(uint16_t x, uint16_t y, char ch);

private:
  void send8BitData(uint8_t data);
  void writeBus(uint8_t data);

  virtual void setAddress(uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1);
  virtual void sendCommand(uint8_t index);
  virtual void sendData(uint16_t data);
  
  uint16_t _cols;
  uint16_t _rows;

  uint8_t _sck, _miso, _mosi, _cs;
  
  regtype *_pinSCK, *_pinMISO, *_pinMOSI;
  regsize _bitmaskSCK, _bitmaskMISO, _bitmaskMOSI;
  uint8_t _spiSettings;
};


#endif

