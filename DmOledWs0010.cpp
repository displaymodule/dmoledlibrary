/**********************************************************************************************
 Copyright (c) 2015 DisplayModule. All rights reserved.

 Redistribution and use of this source code, part of this source code or any compiled binary
 based on this source code is permitted as long as the above copyright notice and following
 disclaimer is retained.

 DISCLAIMER:
 THIS SOFTWARE IS SUPPLIED "AS IS" WITHOUT ANY WARRANTIES AND SUPPORT. DISPLAYMODULE ASSUMES
 NO RESPONSIBILITY OR LIABILITY FOR THE USE OF THE SOFTWARE.
 ********************************************************************************************/

#include "DmOledWs0010.h"

DmOledWs0010::DmOledWs0010(uint8_t sck, uint8_t miso, uint8_t mosi, uint8_t cs):DmTftBase(20, 2)
{
  _cs = cs;
  _sck = sck;
  _miso = miso;
  _mosi = mosi;
}

DmOledWs0010::~DmOledWs0010() 
{
}

void DmOledWs0010::writeBus(uint8_t data)
{
	uint8_t count=0;
	uint8_t temp = data;

  	for(count=0;count<8;count++) {
  		if(temp&0x80) {
  			sbi(_pinMOSI, _bitmaskMOSI);
 		}
  		else {
   			cbi(_pinMOSI, _bitmaskMOSI);
  		}

		temp=temp<<1;
		pulse_low(_pinSCK, _bitmaskSCK);
	} 
}

void DmOledWs0010::sendCommand(uint8_t index) 
{
  	cbi(_pinCS, _bitmaskCS);
  
  	sbi(_pinSCK, _bitmaskSCK);  
  	cbi(_pinMOSI, _bitmaskMOSI);  // rs bit = 0
  	pulse_low(_pinSCK, _bitmaskSCK);
  	cbi(_pinMOSI, _bitmaskMOSI);  // rw bit = 0
  	pulse_low(_pinSCK, _bitmaskSCK);
 	writeBus(index);

   	sbi(_pinCS, _bitmaskCS);
}

void DmOledWs0010::send8BitData(uint8_t data)
{
  	cbi(_pinCS, _bitmaskCS);
  
  	sbi(_pinSCK, _bitmaskSCK);  
  	sbi(_pinMOSI, _bitmaskMOSI);  // rs bit = 1
  	pulse_low(_pinSCK, _bitmaskSCK);
  	cbi(_pinMOSI, _bitmaskMOSI);  // rw bit = 0
  	pulse_low(_pinSCK, _bitmaskSCK);
	writeBus(data);

	sbi(_pinCS, _bitmaskCS);
}

void DmOledWs0010::sendData(uint16_t data) 
{

}
void DmOledWs0010::setAddress(uint16_t x0,uint16_t y0,uint16_t x1,uint16_t y1)
{
}

void DmOledWs0010::setCursor(uint8_t x, uint8_t y)
{
 	uint8_t row_offsets[] ={0x00, 0x40, 0x14, 0x54};
	if(y >= _rows){
		y = 0;
		}
	sendCommand((x + row_offsets[y])|0x80);
}

void DmOledWs0010::drawChar(uint16_t x,uint16_t y,char ch)
{
   send8BitData(ch);
}
void DmOledWs0010::drawString(uint8_t x,uint8_t y,const char * p)
{
    setCursor(x, y);
	send8BitData(*p);
	p++;
	
	while(*p!='\0')
	{	
	  if(x > _cols) {
		x = 0;
		y += 1;
	  }
	  if(y >= _rows) {
		y = x = 0;
	  }
	  drawChar(x, y, *p);
	  x += 1;
	  p++;
	  //delay(20);
	}
}

void DmOledWs0010::init(uint8_t x, uint8_t y) {
  _cols = x;
  _rows = y;
  _pinSCK  = portOutputRegister(digitalPinToPort(_sck));
  _bitmaskSCK  = digitalPinToBitMask(_sck);
  _pinMISO  = portOutputRegister(digitalPinToPort(_miso));
  _bitmaskMISO  = digitalPinToBitMask(_miso);
  _pinMOSI  = portOutputRegister(digitalPinToPort(_mosi));
  _bitmaskMOSI  = digitalPinToBitMask(_mosi);  
  _pinCS  = portOutputRegister(digitalPinToPort(_cs));
  _bitmaskCS  = digitalPinToBitMask(_cs);

  pinMode(_cs,OUTPUT);
  pinMode(_sck,OUTPUT);
  pinMode(_mosi,OUTPUT);
  pinMode(_miso,INPUT);

  cbi(_pinCS, _bitmaskCS);
  
  // ws0010 init  
  delay(50);
  sendCommand(0x38);  //function set
  delay(10);  
  sendCommand(0x38);  
  delay(10);    

  sendCommand(0x08);  // display off
  delay(10);
  sendCommand(0x01);  // display clear
  delay(10);
  sendCommand(0x06);  // entry mode set
  delay(10);
  sendCommand(0x02);  //home command
  delay(10);
  sendCommand(0x0C);  // display on

  delay(50);
  sbi(_pinCS, _bitmaskCS);
	
}

/*********************************************************************************************************
  END FILE
*********************************************************************************************************/

