/**********************************************************************************************
 Copyright (c) 2015 DisplayModule. All rights reserved.

 Redistribution and use of this source code, part of this source code or any compiled binary
 based on this source code is permitted as long as the above copyright notice and following
 disclaimer is retained.

 DISCLAIMER:
 THIS SOFTWARE IS SUPPLIED "AS IS" WITHOUT ANY WARRANTIES AND SUPPORT. DISPLAYMODULE ASSUMES
 NO RESPONSIBILITY OR LIABILITY FOR THE USE OF THE SOFTWARE.
 ********************************************************************************************/

#ifndef DM_OLED_SEPS525_h
#define DM_OLED_SEPS525_h

#include "DmTftBase.h"

class DmOledSeps525 : public DmTftBase
{
public:
  DmOledSeps525(uint8_t cs=D10, uint8_t dc=D9, uint8_t rst=D8);
  virtual ~DmOledSeps525();
  void init(void);

private:
  void send8BitData(uint8_t data);
  void writeBus(uint8_t data);

  virtual void setAddress(uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1);
  virtual void sendCommand(uint8_t index);
  virtual void sendData(uint16_t data);
  
  static const uint16_t _width;
  static const uint16_t _height;

  uint8_t _cs, _dc, _rst;
  
  regtype *_pinDC, *_pinRST;
  regsize _bitmaskDC, _bitmaskRST;
  uint8_t _spiSettings;
};


#endif

