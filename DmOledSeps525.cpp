/**********************************************************************************************
 Copyright (c) 2015 DisplayModule. All rights reserved.

 Redistribution and use of this source code, part of this source code or any compiled binary
 based on this source code is permitted as long as the above copyright notice and following
 disclaimer is retained.

 DISCLAIMER:
 THIS SOFTWARE IS SUPPLIED "AS IS" WITHOUT ANY WARRANTIES AND SUPPORT. DISPLAYMODULE ASSUMES
 NO RESPONSIBILITY OR LIABILITY FOR THE USE OF THE SOFTWARE.
 ********************************************************************************************/

#include "DmOledSeps525.h"

#define Max_Column	0x9F			// 160-1
#define Max_Row		0x7F			// 128-1

DmOledSeps525::DmOledSeps525(uint8_t cs, uint8_t dc, uint8_t rst):DmTftBase(160, 128)
{
  _cs = cs;
  _dc = dc;
  _rst = rst;
}

DmOledSeps525::~DmOledSeps525() 
{
}

void DmOledSeps525::writeBus(uint8_t data)
{
  SPCR = _spiSettings;         // SPI Control Register
  SPDR = data;                 // SPI Data Register
  while(!(SPSR & _BV(SPIF)));  // SPI Status Register Wait for transmission to finish
}

void DmOledSeps525::sendCommand(uint8_t index) 
{
  cbi(_pinDC, _bitmaskDC);
  writeBus(index);
  
}

void DmOledSeps525::send8BitData(uint8_t data)
{
  sbi(_pinDC, _bitmaskDC);
  writeBus(data);

}

void DmOledSeps525::sendData(uint16_t data) {
  uint8_t dh = data>>8;
  uint8_t dl = data&0xff;


  sbi(_pinDC, _bitmaskDC);
  writeBus(dh);
  writeBus(dl);

}

void DmOledSeps525::setAddress(uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1) {
  sendCommand(0x17); // set range  
  sendData(x0);
  sendCommand(0x18);
  sendData(x1);
  sendCommand(0x19);  
  sendData(y0);
  sendCommand(0x1A); 
  sendData(y1);

  sendCommand(0x20); // set display offset
  sendData(x0);
  sendCommand(0x21);
  sendData(y0);  

  sendCommand(0x22); // set write RAM
}

void DmOledSeps525::init(void) {
  setTextColor(BLACK, WHITE);
  _pinCS  = portOutputRegister(digitalPinToPort(_cs));
  _bitmaskCS  = digitalPinToBitMask(_cs);
  _pinDC  = portOutputRegister(digitalPinToPort(_dc));
  _bitmaskDC  = digitalPinToBitMask(_dc);
  _pinRST  = portOutputRegister(digitalPinToPort(_rst));
  _bitmaskRST  = digitalPinToBitMask(_rst);  
  pinMode(_cs,OUTPUT);
  pinMode(_dc,OUTPUT);
  pinMode(_rst,OUTPUT);

  sbi(_pinCS, _bitmaskCS);
  sbi(_pinDC, _bitmaskDC);
  sbi(_pinRST, _bitmaskRST);

  SPI.begin();
  SPI.setClockDivider(SPI_CLOCK_DIV2); // 8 MHz (full! speed!)
  SPI.setBitOrder(MSBFIRST);
  SPI.setDataMode(SPI_MODE3);
  _spiSettings = SPCR;

  cbi(_pinCS, _bitmaskCS);
  cbi(_pinRST, _bitmaskRST);
  delay(10); 
  sbi(_pinRST, _bitmaskRST);
  delay(10); 

  // Seps525 init
  sendCommand(0x04);send8BitData(0x01);
  delay(2);
  sendCommand(0x04);send8BitData(0x00);
  delay(1);
  sendCommand(0x06);send8BitData(0x00);
  sendCommand(0x02);send8BitData(0x01);
  sendCommand(0x03);send8BitData(0x30);
  sendCommand(0x28);send8BitData(0x7F);
  sendCommand(0x29);send8BitData(0x00);
  sendCommand(0x14);send8BitData(0x31);
  sendCommand(0x16);send8BitData(0x66);
  sendCommand(0x10);send8BitData(0x45);
  sendCommand(0x11);send8BitData(0x34);
  sendCommand(0x12);send8BitData(0x33);
  sendCommand(0x08);send8BitData(0x04);
  sendCommand(0x09);send8BitData(0x05);
  sendCommand(0x0A);send8BitData(0x05);
  sendCommand(0x0B);send8BitData(0x9D);
  sendCommand(0x0C);send8BitData(0x8C);
  sendCommand(0x0D);send8BitData(0x57);
  sendCommand(0x80);send8BitData(0x00);
  sendCommand(0x13);send8BitData(0x00);
  
  sendCommand(0x06);send8BitData(0x01);

  delay(50);
  sbi(_pinCS, _bitmaskCS);
  clearScreen();
}




/*********************************************************************************************************
  END FILE
*********************************************************************************************************/

